<?php
// require "../kint.phar";
// d($_GET);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Météo</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/main.js"></script>
</head>

<body>
    <div class="grid grid-1">
        <h1>APPLICATION MÉTÉO</h1>

        <a href="lib/saisie.php" class="form">Formulaire de saisie</a>

        <br />

        <div class="grid grid-1-1-1-1-1">
            <?php
            include "lib/filtres.php";

            echo "<a href='index.php'>Aucun filtre</a> ";

            foreach ($filtres as $filtre) :
                echo '<a href=index.php?val_min=' . $filtre['val_min'] . '&val_max=' . $filtre['val_max'] . '>' . $filtre['libelle'] . '</a>';
            endforeach;
            ?>
        </div>

        <br />

        <?php
        $f = fopen('../datas/datas-meteo.csv', 'r');

        $datas = [];

        while (!feof($f)) {
            $datas[] = fgetcsv($f, 0, ";");
        }

        // d($datas);
        echo "<ul class='grid grid-1-1-1 liste-sans-puce'>";

        foreach ($datas as $ligne) :
            if ($ligne !== false) {
                if (isset($_GET['val_min']) === false && isset($_GET['val_max']) === false) {
                    echo "<li><div class='placeholder-block'><div>" . $ligne[0] . '</div><br/><div>' . $ligne[1] . "°</div> <div><img src='$ligne[2]'></div></div></li>";
                } else {
                    if (filter_input(INPUT_GET, 'val_min', FILTER_VALIDATE_FLOAT) !== false && filter_input(INPUT_GET, 'val_max', FILTER_VALIDATE_FLOAT) !== false) {
                        if ($ligne[1] >= $_GET['val_min'] && $ligne[1] < $_GET['val_max']) {
                            echo "<li><div class='placeholder-block'><div>" . $ligne[0] . '</div><br/><div>' . $ligne[1] . "°</div> <div><img src='$ligne[2]'></div></div></li>";
                        }
                    }
                }
            }
        endforeach;

        echo "</ul>";

        fclose($f);

        if (isset($_GET['status'])) {
            if ($_GET['status'] === 'success') {
                echo '<div id="snackbar" style="background-color: darkgreen">Succés !</div>';
            } else {
                echo '<div id="snackbar" style="background-color: darkred">Erreur !</div>';
            }
            echo '<script>myFunction()</script>';
        }
        ?>
    </div>

</body>

</html>