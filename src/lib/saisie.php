<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Météo</title>
  <link rel="stylesheet" href="../css/style.css">
</head>

<body>
  <div class="grid">
    <h1>Formulaire de saisie</h1>

    <form action="validation.php" method="post" enctype="multipart/form-data">
      <div class="grid">
        <label for="town" class="lab">Ville :</label>
        <input type="text" id="town" name="town_name">
      </div>

      <br />

      <div class="grid">
        <label for="temperature" class="lab">Température :</label>
        <input type="number" step="0.1" id="temperature" name="town_temperature">
      </div>

      <br />

      <div class="grid">
        <p>Ensoleillement :</p>

        <div class="grid grid-1-1-1">
          <div class="placeholder-block">
            <div>
              <input type="radio" id="soleil" name="town_sunshine" value="sunshine_sun">
              <label for="soleil">Soleil</label>
            </div>
            <br/>
            <div>
              <img src="https://www.icone-png.com/png/11/10666.png" alt="icone soleil">
            </div>
          </div>

          <div class="placeholder-block">
            <div>
              <input type="radio" id="pluie" name="town_sunshine" value="sunshine_rain">
              <label for="pluie">Pluie</label>
            </div>
            <br/>
            <div>
              <img src="https://www.icone-png.com/png/11/10618.png" alt="icone pluie">
            </div>
          </div>

          <div class="placeholder-block">
            <div>
              <input type="radio" id="eclaircie" name="town_sunshine" value="sunshine_bright">
              <label for="eclaircie">Éclaircie</label>
            </div>
            <br/>
            <div>
              <img src="https://www.icone-png.com/png/11/10712.png" alt="icone éclaircie">
            </div>
          </div>

          <div class="placeholder-block">
            <div>
              <input type="radio" id="brouillard" name="town_sunshine" value="sunshine_fog">
              <label for="brouillard">Brouillard</label>
            </div>
            <br/>
            <div>
              <img src="https://www.icone-png.com/png/11/10776.png" alt="icone brouillard">
            </div>
          </div>

          <div class="placeholder-block">
            <div>
              <input type="radio" id="neige" name="town_sunshine" value="sunshine_snow">
              <label for="neige">Neige</label>
            </div>
            <br/>
            <div>
              <img src="https://www.icone-png.com/png/11/10919.png" alt="icone neige">
            </div>
          </div>

          <div class="placeholder-block">
            <div>
              <input type="radio" id="orage" name="town_sunshine" value="sunshine_storm">
              <label for="orage">Orage</label>
            </div>
            <br/>
            <div>
              <img src="https://www.icone-png.com/png/35/35368.png" alt="icone orage">
            </div>
          </div>

          <div class="placeholder-block">
            <div>
              <input type="radio" id="nuageux" name="town_sunshine" value="sunshine_cloudy">
              <label for="nuageux">Nuageux</label>
            </div>
            <br/>
            <div>
              <img src="https://www.icone-png.com/png/11/10835.png" alt="icone nuages">
            </div>
          </div>
        </div>
      </div>

      <br />

      <div class="button_submit">
        <button type="submit">Envoyer</button>
      </div>
    </form>
  </div>

</body>

</html>